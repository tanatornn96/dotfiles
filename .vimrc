set nocompatible              " be iMproved, required
filetype off

filetype plugin on

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'Valloric/YouCompleteMe'
Plugin 'kien/ctrlp.vim'
Plugin 'ntpeters/vim-better-whitespace'
Plugin 'leafgarland/typescript-vim'
Plugin 'mxw/vim-jsx'
Plugin 'jiangmiao/auto-pairs'
Plugin 'lervag/vimtex'
Plugin 'joshdick/onedark.vim'

call vundle#end()            " required

set laststatus=2

syntax on
filetype plugin indent on

command! E Explore


set tabstop=2 " visual spaces per tab
set softtabstop=2 " spaces in tab when editing
set expandtab "use spaces instead of tabs
set shiftwidth=2

set number " show line numbers
set cursorline

let g:onedark_termcolors=16
colorscheme onedark

set runtimepath^=~/.vim/bundle/ctrlp.vim

" Searching
set ignorecase " case insensitive
set incsearch " search while typing
set hlsearch " highlight matches

" Backup files
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

python from powerline.vim import setup as powerline_setup
python powerline_setup()
python del powerline_setup


